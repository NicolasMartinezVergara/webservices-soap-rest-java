Proyecto del curso “Universidad Java 2021, de cero a experto” (UDEMY)
Proyecto centrado en desarrollar webservices de tipo REST y tipo SOAP.

El objetivo de este ejercicio fue realizar una aplicación web para gestionar una lista de personas, en donde se puedan mostrar, agregar,
modificar y eliminar los registros.
La idea fue combinar la creación del backend con Java EE aplicando el concepto de servicios web de tipo RESTfull y tipo SOAP para
conectar a nuestro backend con nuestro cliente.
Contiene además dos aplicaciones Java para simular estos clientes de tipo SOAP y de tipo RESTfull, dentro del paquete de Test de cada
aplicación mediante método Main.
La conexión a MySQL utiliza JTA para la persistencia de datos, y EJB´s (Enterprise Java Beans - clases de tipo Stateless) para las capas de negocio y dao, de esta
manera pueden inyectar dependencias y contextos de persistencia.

Backend:
Desarrollado con Apache Netbeans, utilizando Java Empresarial (JavaEE version 8), y JDK 1.8.
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
MySQL en localhost:3306 .
Capa de datos utilizando JTA.
Conexión de tipo REST por medio de la librería jersey-client de Glassfish Jersey Core.
Conexión SOAP por medio de javax.ws
Todas estas dependencias del proyecto descargadas mediante la configuración del archivo pom.xml.
El servidor de aplicaciones es Glassfish, versión 5.0.
Servidor en localhost:8080 .

La configuración de conexión a la base de datos está realizada en el archivo persistence.xml (webservices-soap-rest-java/app-web-listado-personas-WS/src/main/resources/META-INF/persistence.xml), 
declarando también la unidad de persistencia como "PersonaPU", y el jta-data-source (para encontrar el JNDI al pool de conexiones creado, todo
desde el servidor de Glassfish)

La clase de dominio o modelo está definida como @Entity para que JTA pueda relacionarla con los registros de la base de datos.
Cada objeto tiene su atributo id (o llave primaria de acceso), que será creada de manera automática y autoincrementable desde la base de datos 
por cada registro creado (de ahí la anotación @GeneratedValue en el atributo de Id).
Se generan los métodos "get" y "set" de cada atributo, más el método toString.

En la capa de datos, dentro de la ruta webservices-soap-rest-java/app-web-listado-personas-WS/src/main/java/ar/com/listado/data, 
se encuentran todas las clases DAO de cada entidad.
En este caso sólo se define la interface de PersonaDAO y la clase que implementa esta interface.
En la clase PersonaDAOImpl se encuentran los métodos para interactuar con la base de datos: get(mediante el método getResultList
del objeto Query, creado en el mismo método), persist (para insertar un nuevo registro), merge (para actualizar un registro) y delete (realizando
primero un merge del objeto, y luego delete).
Al ser declarada esta clase como un EJB con la anotación @Stateless, permite inyectar la unidad de persistencia de la base de datos (@PersistenceContext),
y de esta forma inicializar nuestro atributo de Entity Manager para acceder a los métodos que reciben y envían información con la base de datos.
El uso de EJB permite además un mejor manejo transaccional.


Siguiendo con la capa de negocio en la ruta webservices-soap-rest-java/app-web-listado-personas-WS/src/main/java/ar/com/listado/service, 
se encuentra la interface de PersonaService más su implementación (respetando el concepto de alta cohesión y bajo acoplamiento entre componentes).
En la interface se define la firma de los métodos (en este caso, es una interface local - @Locale).
En la clase de implementación, se implemente la interface y se define con la anotación @Stateless para ser considerada un EJB y así poder inyectar una instancia de 
nuestra clase PersonaDao (la implementación de esta interface), mediante la anotación de @Inject, y así poder acceder a sus métodos.
Se crea además otra interface para nuestro cliente SOAP (PersonaServiceWS), con la anotación de @Remote para ser accedida, y declarando el/los métodos que serán
luego expuestos por nuestra clase de implementación (con la anotación @WebMethod sobre cada método de esta interface). 
A su vez, esta implementación debe extender también de esta interface remota.

Creo luego la implementación de PersonaServiceRS, pero en este caso, será una clase para realizar nuestra API de tipo RESTfull, exponiendo
los métodos de la capa de negocio,
En primer lugar se agrega la anotación de @Path("") indicando el string con el cual se podrá ingresar por url (en este caso, "/personas").
Agregamos la anotación @Stateless para poder inyectar una instancia de nuestra clase PersonaServiceImpl, mediante la anotación de @Inject, 
y así poder acceder a sus métodos, los cuales serán llamados dentro de los métodos de esta API.
Por cada método, se antepone el tipo de petición HTTP que representa ese método por medio de anotaciones (@GET, @POST, @PUT, @DELETE),
así como también especificar por medio de las anotaciones de @Consumes y @Produces el tipo de información que consume y produce este web
service (puede ser de tipo XML y/o de tipo JSON, dependiendo el caso).
Al generar información de tipo XML (marshalling-de objeto a XML), nos permite también acceder a los WSDL (Web Service Description Language) 
para importarlo luego en la aplicaciónde cliente SOAP (junto con los archivos XSD -SML Schema- que validan nuestros XML).

Para configurar el servlet de Jersey (para servicio RESTfull), puede hacerse por medio de una clase Java o por medio del archivo web.xml.
En este caso, fue realizado mediante el archivo web.xml en la ruta webservices-soap-rest-java/app-web-listado-personas-WS/src/main/webapp/WEB-INF/web.xml .
En el tag servlet-mapping indicamos el servlet-name ("JerseyWebApplication") y el url-pattern (/webservice/*) con el cual se podrá ingresar por url 
(en este caso, "webservice", y luego el path de "/personas" correspondiente a la clase del rest web service).


Aplicaciones Java Clientes REST y SOAP:
En nuestro cliente SOAP, se configura el archivo el archivo pom.xml, y mediante la acción de "goals" de Maven, generamos de manera automática
las clases necesarias (de entidad y de servicio) para acceder al web service dentro de nuestra aplicación (que luego podemos accederlas en nuestro método main para
pruebas que se van ejecutando, arrojando los resultados por consola).

Para simular nuestro cliente RESTfull, se debe configurar el archivo porm.xml y agregar la librería de Jersey Client.
La clase de entidad, en lugar de importarla, la creamos manualmente.
Y en el método main utilizaremos el URL base de nuestro web service para construir y enviar las peticiones, recibiendo los resultados
en nuestra consola.
