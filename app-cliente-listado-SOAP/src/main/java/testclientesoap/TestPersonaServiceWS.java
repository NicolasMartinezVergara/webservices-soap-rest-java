
package testclientesoap;

import clientesoap.service.Persona;
import clientesoap.service.PersonaServiceImplService;
import clientesoap.service.PersonaServiceWS;
import java.util.List;


public class TestPersonaServiceWS {
    
    public static void main(String[] args) {
        
        PersonaServiceWS personaService = new PersonaServiceImplService().getPersonaServiceImplPort();
        
        System.out.println("Ejecutando SOAP Web Service para leer todas las personas de la lista:");
        List<Persona> personas = personaService.listarPersonas();
        for(Persona persona : personas){
            System.out.println("IdPersona: " + persona.getIdPersona() + " Nombre: " + persona.getNombre() + " " + persona.getApellido());
        }
        System.out.println("Fin del llamado al servicio");
                
                
    }
    
}
