
package ar.com.listado.test;

import ar.com.listado.domain.Persona;
import java.util.List;
import javax.ws.rs.client.*;
import javax.ws.rs.core.*;



public class TestPersonaServiceRS {
    
    private static final String URL_BASE = "http://localhost:8080/ListadoPersonasWS/webservice";
    private static Client cliente;
    private static WebTarget webTarget;
    private static Persona persona;
    private static List<Persona> personas;
    private static Invocation.Builder invocationBuilder;
    private static Response response;
    
    public static void main(String[] args) {
        
        System.out.println("Ejecutando REST Web Service para encontrar una persona por Id:");
        cliente = ClientBuilder.newClient();
        webTarget = cliente.target(URL_BASE).path("/personas");
        persona = webTarget.path("/3").request(MediaType.APPLICATION_XML).get(Persona.class);
        System.out.println("Persona recuperada: " + persona);
        
        System.out.println("\nEjecutando REST Web Service para leer todas las personas de la lista:");
        personas = webTarget.request(MediaType.APPLICATION_XML).get(Response.class).readEntity(new GenericType<List<Persona>>(){});
        System.out.println("Personas recuperadas:");
        imprimirPersonas(personas);
        
        System.out.println("\nEjecutando REST Web Service para agregar una persona a la lista:");
        Persona nuevaPersona = new Persona();
        nuevaPersona.setNombre("Marcos");
        nuevaPersona.setApellido("Gutierrez");
        nuevaPersona.setEmail("mgutierrez@mail.com");
        nuevaPersona.setTelefono("1144556677");
        
        invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        response = invocationBuilder.post(Entity.entity(nuevaPersona, MediaType.APPLICATION_XML));
        System.out.println("");
        System.out.println(response.getStatus());
        //Recupero persona recién agregada
        Persona personaRecuperada = response.readEntity(Persona.class);
        System.out.println("\nPersona agregaada: " + personaRecuperada);
        
        //Modifico persona recuperada anteriormente
        System.out.println("\nEjecutando REST Web Service para modificar a una persona de la lista:");        
        Persona personaModificar = personaRecuperada;
        personaModificar.setNombre("Mariano");
        String pathId = "/" + personaModificar.getIdPersona();
        invocationBuilder = webTarget.path(pathId).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.put(Entity.entity(personaModificar, MediaType.APPLICATION_XML));
        System.out.println("");
        System.out.println(response.getStatus());
        System.out.println("Persona modificada: " + response.readEntity(Persona.class));
        
        //Elimino a la persona recuperada anteriormente        
        System.out.println("\nEjecutando REST Web Service para eliminar a una persona de la lista:");
        Persona personaEliminar = personaRecuperada;
        String pathEliminarId = "/" + personaEliminar.getIdPersona();
        invocationBuilder = webTarget.path(pathEliminarId).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.delete();
        System.out.println("");
        System.out.println(response.getStatus());
        System.out.println("Persona Eliminada: " + personaEliminar);
        
        
        
    }
    
    private static void imprimirPersonas(List<Persona> personas){
        for(Persona p : personas){
            System.out.println("Persona: " + p);
        }
    }
    
}
