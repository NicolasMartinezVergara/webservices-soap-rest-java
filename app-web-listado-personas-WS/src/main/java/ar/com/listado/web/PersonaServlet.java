
package ar.com.listado.web;

import ar.com.listado.domain.Persona;
import ar.com.listado.service.PersonaService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/personas")
public class PersonaServlet extends HttpServlet{
    
    @Inject
    PersonaService personaService;
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        List<Persona> personas = personaService.listarPersonas();
        
        request.setAttribute("personas", personas);
        request.getRequestDispatcher("/listado/listadoPersonas.jsp").forward(request, response);
        
    }
}
